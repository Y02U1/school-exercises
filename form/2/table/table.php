<?php
$max = $_POST['max'];
?>

<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Tavola pitagorica fino a <?php echo $max; ?></title>
        <link type="text/css" rel="stylesheet" href="main.css">
    </head>
    <body>
        <?php if ($max < 5 || $max == "" || $max > 30): ?>
            <h1>Errore</h1>
            <p>
                <b>I dati inviati al server non sono validi!</b> Per favore, riprova a
                <a href="../index.html">compilare la form</a>.
            </p>
        <?php else: ?>
            <table>
                <tr>
                    <?php
                    echo "<tr>";
                    echo "<th>X</th>";
                    for ($i=1; $i <= $max; $i++) {
                        echo "<th scope='col'>$i</th>";
                    }
                    echo "</tr>";
                    ?>
                </tr>
                <?php
                // Itera tra i numeri passati in GET (compresi) calcolando e mostrando il fattoriale a lato
                for ($i=1; $i <= $max; $i++) {
                    echo "<tr scope='row'>";
                    echo "<th>$i</th>";
                    for ($j=1; $j <= $max ; $j++) {
                        echo "<td>".($i*$j)."</td>";
                    }
                    echo "</tr>";
                }
                ?>
            </table>
        <?php endif; ?>
    </body>
</html>
