<?php
/*
 * Leggi il file dei prodotti --> per ogni linea crea un prodotto --> stampa gli attributi del prodotto in una tabella
 *      E usa un accumulatore per calcolare il totale
 */
session_start();
?>

<html>
<head>
    <title>ProdAzienda</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<nav>
    <?php if(isset($_SESSION["nome"]) && isset($_SESSION["cognome"])): ?>
        <p>
            Sei loggato come <b><?=$_SESSION["nome"]." ".$_SESSION["cognome"]?></b>
            <a href="index.php" class="button" role="button">Back</a>
            <a href="carrello.php" class="button-secondary" role="button">Carrello</a>
        </p>
    <?php else: ?>
        <a href="login.php" class="button" role="button">Login</a>
    <?php endif; ?>
</nav>
<table>
    <tr>
        <th scope="col">Nome</th>
        <th scope="col">Genere</th>
        <th scope="col">Immagine</th>
        <th scope="col">Prezzo</th>
        <th scope="col">Giacenza</th>
    </tr>
    <?php
    require_once "config.php";
    require_once "Prodotto.php";
    $total = 0;
    foreach (file(CSV_PRODOTTI) as $line) {
        $prodotto = new Prodotto();
        $prodotto->setCSV($line);
        $total += $prodotto->getPrezzo()*$prodotto->getGiacenza();
        ?>
        <tr>
            <td><?=$prodotto->getNome()?></td>
            <td><?=$prodotto->getGenere()?></td>
            <td>
                <img src="<?=$prodotto->getImmagine()?>">
            </td>
            <td><?=$prodotto->getPrezzo()?> €</td>
            <td><?=$prodotto->getGiacenza()?></td>
            <?php if(isset($_SESSION["nome"]) && isset($_SESSION["cognome"])): ?>
                <td>
                    <form action="carrello.php" method="POST">
                        <label for="qt">N°</label>
                        <input name="id" type="hidden" value="<?=$prodotto->getId()?>" required="required">
                        <input id="qt" name="qt" type="number" min="0" max="<?=$prodotto->getGiacenza()?>" required="required">
                        <?php if($prodotto->getGiacenza() == 0): ?>
                            <button type="submit" name="add" disabled>Aggiungi al carrello</button>
                        <?php else: ?>
                            <button type="submit" name="add">Aggiungi al carrello</button>
                        <?php endif; ?>
                    </form>
                </td>
            <?php endif; ?>
        </tr>
    <?php } ?>
</table>
<p>
    Totale: <?=$total?> €
</p>
</body>
</html>