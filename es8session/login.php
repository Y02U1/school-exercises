<?php
    /*
    * Apri la sessione
    * Se la sessione è impostata --> distruggi la sessione e reindirizza alla home (Logout)
    * ...Altrimenti --> fai login
    *     Se successo --> imposta i parametri della sessione (Login vero e proprio)
    *     ...Altrimenti --> informa l'utente del login fallito
    */
    require_once "config.php";
    session_start();
    if (isset($_SESSION["nome"])) {
        // Logout
        session_destroy();
        header("Location: index.php");
        die();
    }
    if (isset($_POST["username"]) && isset($_POST["password"])) {
        // Login
        require_once "Utente.php";
        $user = new Utente(CSV_UTENTI);
        if($user->login($_POST["username"], $_POST["password"])) {
            $_SESSION["nome"]       = $user->getNome();
            $_SESSION["cognome"]    = $user->getCognome();
            $_SESSION["username"]   = $user->getUsername();
            header("Location: index.php");
        }
    }
?>
<html>
    <head>
        <title>ProdAzienda - login</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
        <nav>
            <a href="index.php" class="button" role="button">Back</a>
        </nav>
        <h1>Login</h1>
        <?php if(isset($_POST["username"]) && isset($_POST["password"])): ?>
            <p>Errore nel login!</p>
        <?php endif; ?>
        <form method="POST">
            <input type="text" name="username" placeholder="Username" required="required">
            <input type="password" name="password" placeholder="Password" required="required">
            <button name="login" type="submit">Login</button>
        </form>
    </body>
</html>