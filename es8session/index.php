<?php
/*
 * Apri la sessione
 * Se loggato -->
 *      Mostra logout
 *      Mostra inserimento prodotti
 * Se ! loggato -->
 *      Mostra login
 * In entrambi i casi -->
 *      Mostra stampa genere
 *      Mostra stampa magazzino
 */
session_start();
?>
<html>
    <head>
        <title>ProdAzienda</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
        <nav>
            <?php if(isset($_SESSION["nome"]) && isset($_SESSION["cognome"])): ?>
                <p>
                    Sei loggato come <b><?=$_SESSION["nome"]." ".$_SESSION["cognome"]?></b>
                    <a href="login.php" class="button">Effettua il logout</a>
                    <a href="carrello.php" class="button-secondary" role="button">Carrello</a>
                </p>
            <?php else: ?>
                <a href="login.php" class="button" role="button">Login</a>
            <?php endif; ?>
        </nav>
        <main>
            <ul>
                <li>
                    <a href="magazzino.php" class="button" role="button">Calcola valore totale articoli nel magazzino</a>
                </li>
                <li>
                    <a href="genere.php" class="button" role="button">Cerca prodotti per genere</a>
                </li>
                <?php if(isset($_SESSION["nome"])): ?>
                    <li>
                        <a href="inserisci.php" class="button" role="button">Inserisci prodotti</a>
                    </li>
                <?php endif; ?>
            </ul>
        </main>
    </body>
</html>