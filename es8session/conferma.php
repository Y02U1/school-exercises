<?php
session_start();
if (!isset($_SESSION["nome"]) && !isset($_SESSION["cognome"]) && !isset($_COOKIE["carrello"])) {
    header("Location: index.php");
    die();
} else {
    require_once "Cart.php";
    $cart = new Cart($_COOKIE["carrello"]);
    $cart->buy($_SESSION["username"]);
    setcookie("carrello", "");
}
?>
<html>
    <head>
        <title>Acquisto effettuato</title>
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
        <h1>Ordine portato a termine con successo</h1>
        <a href="carrello.php" role="button" class="button">Torna indietro</a>
    </body>
</html>
