<?php
/*
 * Apri la sessione
 * Se non è impostato qualcosa --> l'utente non è loggato --> reindirizza alla home
 *
 * ...Altrimenti -->
 *      Controlla che siano stati passati tutti i parametri
 *      Controlla che l'immagine sia un'immagine
 *          Se va tutto bene -->
 *              Crea il percorso dell'immagine come: img/<timestamp>_<basename>
 *                  e salva l'immagine
 *              Inizializza un prodotto con i parametri passati
 *                  e scrivi la stringa CSV nel file
 *      ...Se non va bene --> imposta una variabile per informare l'utente dell'errore
 */

session_start();
if (!isset($_SESSION["nome"])) {
    header("Location: index.php");
    die();
} else if (isset($_POST["nome"]) && isset($_POST["genere"]) && isset($_POST["prezzo"]) && isset($_POST["giacenza"])) {
    require_once "Prodotto.php";
    require_once "config.php";
    if (getimagesize($_FILES["img"]["tmp_name"]) !== false) {
        // IMG
        $fullname = "img/".time()."_".basename($_FILES['img']['name']);
        if (move_uploaded_file($_FILES['img']['tmp_name'], $fullname)) {
            $prodotto = new Prodotto();

            $prodotto->init(next_id(CSV_PRODOTTI), $_POST["nome"], $_POST["genere"], $_POST["prezzo"], $_POST["giacenza"], $fullname);
            $file = fopen(CSV_PRODOTTI, "a");
            $status = fwrite($file, $prodotto->getCSV()."\n");
            fclose($file);
        } else {
            $status = false;
        }
    } else {
        $status = false;
    }
}
?>
<html>
<head>
    <title>ProdAzienda</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<nav>
    <?php if(isset($_SESSION["nome"]) && isset($_SESSION["cognome"])): ?>
        <p>
            Sei loggato come <b><?=$_SESSION["nome"]." ".$_SESSION["cognome"]?></b>
            <a href="index.php" class="button" role="button">Back</a>
            <a href="carrello.php" class="button-secondary" role="button">Carrello</a>
        </p>
    <?php else: ?>
        <a href="login.php" class="button" role="button">Login</a>
    <?php endif; ?>
</nav>
<main>
<h1>Inserisci nuovo prodotto</h1>
<?php if(isset($_POST["nome"]) && isset($_POST["genere"]) && isset($_POST["prezzo"]) && isset($_POST["giacenza"])): ?>
    <?php if($status === false): ?>
        <p>Errore durante il salvataggio del record</p>
    <?php else: ?>
        <p>Record salvato con successo</p>
    <?php endif; ?>
<?php endif; ?>
<form method="POST" enctype="multipart/form-data" onsubmit="return validate(this);">
    <table>
        <tr>
            <td><label for="nome">Nome</label></td><td><input id="nome" name="nome" type="text" placeholder="Nome..." required="required"></td>
        </tr>
        <tr>
            <td><label for="genere">Genere</label></td><td><input id="genere" name="genere" type="text" placeholder="Genere..." required="required"></td>
        </tr>
        <tr>
            <td><label for="prezzo">Prezzo</label></td><td><input id="prezzo" name="prezzo" type="number" step=".01" min="0" placeholder="Prezzo..." required="required"></td>
        </tr>
        <tr>
            <td><label for="giacenza">Giacenza</label></td><td><input id="giacenza" name="giacenza" type="number" min="0" placeholder="Giacenza..." required="required"></td>
        </tr>
        <input type="hidden" name="MAX_FILE_SIZE" value="3000000">
        <tr>
            <td><label for="img">Immagine</label></td><td><input id="img" name="img" type="file" required="required"></td>
        </tr>
        <tr>
            <td colspan="2"><button type="submit">Conferma i dati</button></td>
        </tr>
    </table>
</form>
</main>
<script src="js/validation.ir"></script>
</body>
</html>