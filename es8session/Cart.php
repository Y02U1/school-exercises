<?php

require_once "config.php";
require_once "Prodotto.php";

class Cart
{
    private $cart;

    /**
     * Cart constructor.
     * @param $cart
     */
    public function __construct($cookie = null)
    {
        $cart = array();
        // Se il cookie non è vuoto...
        if (!is_null($cookie)) {
            // ...idrata un array con: id => qt (ovvero il contenuto del cookie)
            foreach (explode("-", $cookie) as $articolo) {
                $pair = explode("_", $articolo);
                $cart[$pair[0]] = $pair[1];
            }
        }
        $this->cart = $cart;
    }

    /**
     * @return array
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * Aggiungi un nuovo prodotto al carrello
     * @param Prodotto $prodotto
     * @param $id
     * @param $qt
     */
    public function add(Prodotto $prodotto, $qt)
    {
        /*
         * Controlla se il prodotto è già presente nel carrello
         *  E
         * La quantità è uguale a quella da cancellare
         */
        if (array_key_exists($prodotto->getId(), $this->cart) && $this->cart[$prodotto->getId()] == $qt) {
            // Il prodotto è già presente, aggiorna il valore
            $this->cart[$prodotto->getId()] = $this->cart[$prodotto->getId()]+$qt;
        } else {
            // Il prodotto non c'è
            $this->cart[$prodotto->getId()] = $qt;
        }
        $this->update_main_csv($prodotto, -$qt);
    }

    public function remove(Prodotto $prodotto, $qt)
    {
        // Controlla se il prodotto è già presente nel carrello
        if (array_key_exists($prodotto->getId(), $this->cart)) {
            // Il prodotto è presente, toglilo
            $this->cart[$prodotto->getId()] = null;
            $this->cart = array_filter($this->cart);
            $this->update_main_csv($prodotto, $qt);
            return true;
        } else {
            // Il prodotto non c'è - ERRORE :(
            return false;
        }
    }

    /**
     * Converti in stringa "CSV" (formato personalizzato id_qt-id_qt-...)
     */
    public function to_csv()
    {
        // Controlla se c'è qualcosa nel carrello - error-checking
        if (count($this->cart) == 0) {
            return null;
        } else {
            $articoli = array();
            // Unisci id e qt e mettili in un array
            foreach ($this->cart as $id=>$qt) {
                array_push($articoli, $id."_".$qt);
            }
            // Implodi le coppie id_qt
            return implode("-", $articoli);
        }
    }

    /**
     * Compra gli articoli nel carrello
     */
    public function buy($username)
    {
        $lines = array();
        // Itera tra i prodotti nel file
        foreach (file(CSV_PRODOTTI) as $line) {
            $tmp = new Prodotto();
            $tmp->setCSV($line);
            // Il prodotto è presente nel carrello?
            if (array_key_exists($tmp->getId(), $this->getCart())) {
                $handle = fopen(CSV_ORDINI, "a+");
                fwrite($handle, $username.";".$tmp->getId().";".$this->getCart()[$tmp->getId()].";".$this->getCart()[$tmp->getId()]*$tmp->getPrezzo()."\r\n");
                fclose($handle);
                // Rimuovilo dall'array
                unset($this->getCart()[$tmp->getId()]);
                array_push($lines, $line);
            } else {
                array_push($lines, $line);
            }
        }
        // Aggiorna il file
        file_put_contents(CSV_PRODOTTI, implode("", $lines));
    }

    /**
     * Aggiorna il CSV principale con i nuovi valori
     * @param Prodotto $prodotto
     * @param $qt
     */
    private function update_main_csv(Prodotto $prodotto, $qt)
    {
        // quantità accettabile, aggiorna la giacenza
        $old_str = $prodotto->getCSV();
        $prodotto->setGiacenza($prodotto->getGiacenza()+$qt);
        $new_str = $prodotto->getCSV();
        $new_file_contents = str_replace($old_str, $new_str, file_get_contents(CSV_PRODOTTI));
        file_put_contents(CSV_PRODOTTI, $new_file_contents);
    }
}