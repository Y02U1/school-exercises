<?php
const CSV_UTENTI = "files/utenti.csv";
const CSV_PRODOTTI = "files/prodotti.csv";
const CSV_ORDINI = "files/ordini.csv";

// Leggi univocamente i generi nel file
function read_generi($file) {
    require_once "Prodotto.php";
    $generi = array();
    foreach (file($file) as $line) {
        $genere = explode(";", $line)[2];
        if (!in_array($genere, $generi))
            array_push($generi, $genere);
    }
    return $generi;
}

function next_id($file) {
    $line = '';

    $handle = fopen($file, 'r');
    $cursor = -1;

    fseek($handle, $cursor, SEEK_END);
    $char = fgetc($handle);


    // Trim trailing newline chars of the file
    while ($char === "\n" || $char === "\r") {
        fseek($handle, $cursor--, SEEK_END);
        $char = fgetc($handle);
    }
    // Read until the start of file or first newline char
    while ($char !== false && $char !== "\n" && $char !== "\r") {
        // Prepend the new char
        $line = $char . $line;
        fseek($handle, $cursor--, SEEK_END);
        $char = fgetc($handle);
    }

    $tmp = new Prodotto();
    $tmp->setCSV($line);
    return $tmp->getId()+1;
}