<?php

class Utente
{
    private $username;
    private $nome;
    private $cognome;
    private $email;
    private $csv;

    /**
     * Utente constructor.
     * @param $csv
     */
    function __construct($csv) {
        $this->csv      = $csv;
        $this->username = null;
        $this->nome     = null;
        $this->cognome  = null;
        $this->email    = null;
    }

    /**
     * Controlla se esiste un utente con le credenziali fornite;
     * ritorna un idoneo valore booleano
     *
     * @param $username
     * @param $password
     * @return bool
     */
    function login($username, $password) {
        $lines = file($this->csv);
        foreach ($lines as $line) {
            $fields = explode(";", $line);
            // La password è memorizzata come hash bcrypt, codificata in base64
            if ($fields[0] == $username && password_verify($password, base64_decode($fields[1]))) {
                $this->username     = $username;
                $this->nome         = $fields[2];
                $this->cognome      = $fields[3];
                $this->email        = $fields[4];
                return true;
            }
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getCognome()
    {
        return $this->cognome;
    }

    /**
     * @param mixed $cognome
     */
    public function setCognome($cognome)
    {
        $this->cognome = $cognome;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getCsv()
    {
        return $this->csv;
    }

    /**
     * @param mixed $csv
     */
    public function setCsv($csv)
    {
        $this->csv = $csv;
    }
}