<?php require_once "config.php" ?>
<?php require_once "Studente.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Aggiungi utente</title>
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    <?php include "_navbar.html" ?>
    <main>
        <?php if(isset($_POST["nome"])
            && isset($_POST["cognome"]) && isset($_POST["scuola"])
            && isset($_POST["indirizzo"]) && isset($_POST["nascita"])): ?>
            <?php
            $csv = implode(";", [
                $_POST['nome'],
                $_POST['cognome'],
                $_POST['scuola'],
                $_POST['indirizzo'],
                $_POST['nascita']
            ]);
            $studente = new Studente($csv);
            $handle = fopen(STUDENTI_CSV, "a+");
            if (FALSE === $handle) {
                echo "<h1>Errore di apertura del file</h1>";
            } else if (FALSE === fwrite($handle, $studente->export_csv()."\n")) {
                echo "<h1>Errore nella scrittura su file</h1>";
            } else {
                fclose($handle);
                echo "<h1>Fatto!</h1>";
            }
            ?>
        <?php else: ?>
        <form method="post" onsubmit="validateUser(this.nome.value,
        this.cognome.value, this.scuola.value, this.indirizzo.value, this.nascita.value);">
            <label for="nome">Nome</label>
            <input id="nome" name="nome" type="text" required="required">
            <label for="cognome">Cognome</label>
            <input id="cognome" name="cognome" type="text" required="required">
            <label for="scuola">Scuola</label>
            <input id="scuola" name="scuola" type="text" required="required">
            <label for="indirizzo">Indirizzo</label>
            <input id="indirizzo" name="indirizzo" type="text" required="required">
            <label for="nascita">Data di nascita</label>
            <input id="nascita" name="nascita" type="date" required="required">
            <button type="submit">Aggiungi utente</button>
        </form>
        <?php endif; ?>
    </main>
    <script src="js/validation.ir" type="text/javascript"></script>
</body>
</html>