-- 1. Creazione database
CREATE DATABASE accessmanager;

USE accessmanager;

-- 1. Creazione tabella utenti
CREATE TABLE utenti (
	id smallint NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Nome varchar(30) NOT NULL,
	Cognome varchar(30) NOT NULL,
	Nickname varchar(30) NOT NULL,
	email varchar(30) NOT NULL,
	-- TODO: chiedere chiarimento sull'attributo multivalore
	indirizzo varchar(30) NOT NULL,
	password binary(60) NOT NULL,
	nascita date NOT NULL
);

-- 1. Creazione tabella accessi
-- TODO: posso cambiare il nome?
CREATE TABLE accessi (
	id smallint NOT NULL AUTO_INCREMENT PRIMARY KEY,
	userId smallint REFERENCES utenti(id),
	login datetime NOT NULL,
	logout datetime DEFAULT NULL
);

-- 2. Inserimento utenti
INSERT INTO utenti VALUES
	(NULL, "Ivan", "Ravasi", "ivan", "ivan.ravasi@example.com", "Via delle camelie 69", "test", "2000-04-01"),
	(NULL, "Giorgio", "Mazza", "mazza", "george.mace@example.com", "Via le dita dal naso 666", "test", "1980-12-25"),
	(NULL, "Gabriel", "Ndione", "ndione", "gabri.ndione@example.com", "", "test", "1990-01-06");

-- 3. Login
INSERT INTO accessi VALUES
  (NULL, 2, "2016-02-01 01:01:00", NULL),
  (NULL, 1, "2016-02-15 01:01:00", NULL),
  (NULL, 1, "2016-02-01 01:01:00", NULL),
  (NULL, 1, "2016-02-01 01:01:00", NULL),
  (NULL, 1, "2016-02-30 01:01:00", NULL),
  (NULL, 1, "2016-02-01 01:01:00", NULL),
  (NULL, 1, "2016-02-01 01:01:00", NULL),
  (NULL, 1, "2016-02-01 01:01:00", NULL),
  (NULL, 1, "2016-02-20 01:01:00", NULL),
  (NULL, 1, "2016-02-01 01:01:00", NULL),
  (NULL, 1, "2016-02-01 01:01:00", NULL),
  (NULL, 1, "2016-02-01 01:01:00", NULL),
  (NULL, 1, "2016-02-05 01:01:00", NULL),
  (NULL, 1, "2016-02-30 01:01:00", NULL),
  (NULL, 1, "2016-02-29 01:01:00", NULL),
  (NULL, 1, "2016-02-28 01:01:00", NULL),
  (NULL, 1, "2016-02-26 01:01:00", NULL),
  (NULL, 2, "2009-06-01 01:01:00", NULL),
  (NULL, 2, "2009-06-01 01:01:00", NULL),
  (NULL, 2, "2006-06-05 01:01:00", NULL),
	(NULL, 1, "2019-01-01 01:01:00", NULL);

-- 4. Logout
UPDATE accessi SET logout = "2019-03-10 23:00:00" WHERE id = 1;

-- 5. Cancellazione di un utente
DELETE FROM utenti WHERE Cognome = "Ndione";

-- 6. Cancellazione degli accessi antecedenti una certa data di tutti gli utenti
DELETE FROM accessi WHERE YEAR(login) < 2008;

-- 7. Elenco alfabetico utenti di un determinato anno di nascita
SELECT *
FROM utenti
WHERE YEAR(nascita) = 1980
ORDER BY nome;

-- 8. Elenco accessi di un utente
SELECT *
FROM accessi
WHERE userId = 2;

-- 9. Elenco accessi tra 2 date
SELECT *
FROM accessi
WHERE YEAR(login) BETWEEN 2000 AND 2010;

-- 10. Numero accessi effettuati per ogni giorno
SELECT DATE(login), COUNT(login)
FROM accessi
GROUP BY DATE(login);

-- 11. Seleziona utente con la sessione più lunga
SELECT MAX(TIMESTAMPDIFF(SECOND, accessi.login, accessi.logout)), utenti.Nome
FROM accessi INNER JOIN utenti ON accessi.userId = utenti.id;

-- 12. Elenco alfabetico utenti con +10 accessi in 1 mese
SELECT utenti.Nome,
        COUNT(login) AS numero_sessioni,
        YEAR(login) AS Year, MONTH(login) AS Month
FROM accessi INNER JOIN utenti ON accessi.userId = utenti.id
GROUP BY userId, YEAR(login), MONTH(login)
HAVING numero_sessioni > 10;