<?php

// ---------------- File esterno per le funzioni ----------------

/*
    Funzione ricorsiva per il calcolo del fattoriale di un numero
*/
function fact($n)
{
    // Exit case (per prevenire loop infiniti)
    if ($n == 1) {
        return 1;
    } else {
        // Ricorsione
        return $n * fact($n-1);
    }
}
?>
