<?php
// Richiedi (una volta) le funzioni definite nel file esterno
require_once 'functions.php';
?>

<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Fattoriale 5-45</title>
        <link type="text/css" rel="stylesheet" href="../main.css">
    </head>
    <body>
        <table>
            <tr>
                <th scope="col">N</th><th scope="col">Fattoriale</th>
            </tr>
            <?php
            // Itera tra i numeri 5 e 45 (compresi) calcolando e mostrando il fattoriale a lato
            for ($i=5; $i <= 45; $i++) {
                echo "<tr scope='row'>
                            <td>$i</td>
                            <td>".fact($i)."</td>
                        </tr>";
            }
            ?>
        </table>
    </body>
</html>
