<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>ASCII</title>
        <link type="text/css" rel="stylesheet" href="../main.css">
    </head>
    <body>
        <table>
            <tr>
                <th scope="col">Oct</th><th scope="col">Dec</th><th scope="col">Hex</th><th scope="col">Char</th>
            </tr>
            <?php
            // Itera tra 128 valori (partendo da 0) per mostrare tutti i caratteri ASCII
            for ($i=0; $i < 256; $i++) {
                /*
                    Spiegazione dei campi:
                        ottale
                        decimale
                        esadecimale
                        ASCII (tramite entità HTML)
                */
                echo "<tr scope='row'>
                            <td>".decoct($i)."</td>
                            <td>$i</td>
                            <td>".dechex($i)."</td>
                            <td>&#$i</td>
                        </tr>";
            }
            ?>
        </table>
    </body>
</html>
