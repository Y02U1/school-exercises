<?php
// Apri la sessione
session_start();
// Variabile per il feedback dell'errore
$query_error = null;
// Controlla se l'utente è loggato
if(isset($_SESSION["tipo"])) {
    // Controlla se l'utente può visualizzare questa pagina:
    //  - l'utente è amministrativo
    //  - lo username è passato
    if(isset($_GET["mese"]) && $_SESSION["tipo"]) {
        // Crea la connessione al DB
        require_once "config.php";
        $link = new mysqli(host, username, password, db);
        if (!$link) {
            // Error feedback
            $query_error = "Errore nella creazione del collegamento";
        } else {
            // Prendi gli accessi dell'utente con username X
            $result = $link->query("SELECT username, COUNT(accessi.id) AS numAccessi
FROM accessi INNER JOIN utenti ON accessi.userId = utenti.id
WHERE MONTH(login) = ".$_GET["mese"]."
GROUP BY MONTH(login), YEAR(login)
HAVING COUNT(accessi.id) > 10
ORDER BY username");
            if ($result->num_rows == 0) {
                // Error feedback
                $query_error = "Non è stato trovato nessun utente con più di 10 accessi in questo mese";
            }
        }
        // Altrimenti redirect
    } else header("Location: index.php");
    // Altrimenti redirect
} else header("Location: index.php");
?>
<html>
<head>
    <title>Elenco accessi</title>
</head>
<body>
<?php if(isset($query_error)): ?>
    <h1><?=$query_error?></h1>
<?php else: ?>
    <h1>Elenco accessi</h1>
    <table>
        <tr>
            <th>Username</th><th>Numero accessi</th>
        </tr>
        <?php while($row = $result->fetch_assoc()): ?>
            <tr>
                <td><?=$row["username"]?></td><td><?=$row["numAccessi"]?></td>
            </tr>
        <?php endwhile; ?>
    </table>
<?php endif; ?>
</body>
</html>
