<?php
    session_start();
    require_once "Utente.php";
    $failedLogin = false;

    // Se l'utente è loggato
    if(isset($_SESSION["username"]))
        // Redirect a index.php
        header("Location: index.php");
    else if(isset($_POST["username"]) && isset($_POST["password"])) {
        $utente = new Utente();
        if($utente->login($_POST["username"], $_POST["password"])){
            $_SESSION["id"] = $utente->getId();
            $_SESSION["username"] = $utente->getUsername();
            $_SESSION["password"] = $utente->getPassword();
            $_SESSION["nome"] = $utente->getNome();
            $_SESSION["cognome"] = $utente->getCognome();
            $_SESSION["email"] = $utente->getEmail();
            $_SESSION["indirizzo"] = $utente->getIndirizzo();
            $_SESSION["nascita"] = $utente->getNascita();
            $_SESSION["tipo"] = $utente->getTipo();
            header("Location: index.php");
        } else {
            $failedLogin = true;
        }
    }
?>
<html>
    <head>
        <title>SQLAPP - login</title>
    </head>
    <body>
        <?php if($failedLogin): ?>
            <p>Login fallito! Credenziali errate</p>
        <?php endif; ?>
        <?php if(isset($_GET["logout"])): ?>
            <p><?=$_GET["logout"]?></p>
        <?php endif; ?>
        <main>
            <form method="post">
                <h1>Effettua l'accesso</h1>
                <fieldset>
                    <legend>Immetti le credenziali</legend>

                    <label for="username">Username</label>
                    <input id="username" name="username">
                    <br>
                    <label for="password">Password</label>
                    <input id="password" name="password" type="password">
                    <br>

                    <button type="submit">Login!</button>
                </fieldset>
            </form>
        </main>
    </body>
</html>
