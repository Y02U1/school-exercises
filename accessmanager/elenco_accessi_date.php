<?php

function toDate($d, $m, $y) {
    return date("Y-m-d H:i:s", strtotime($m."/".$d."/".$y));
}

// Apri la sessione
session_start();
// Variabile per il feedback dell'errore
$query_error = null;
// Controlla se l'utente è loggato
if(isset($_SESSION["tipo"])) {
    // Controlla se l'utente può visualizzare questa pagina:
    //  - l'utente è amministrativo
    //  - lo username è passato
    if(isset($_GET["d1"]) && isset($_GET["m1"]) && isset($_GET["y1"]) &&
        isset($_GET["d2"])&& isset($_GET["m2"])&& isset($_GET["y2"]) &&
        $_SESSION["tipo"]) {

        $date1 = toDate($_GET["d1"], $_GET["m1"], $_GET["y1"]);
        $date2 = toDate($_GET["d2"], $_GET["m2"], $_GET["y2"]);

        // Crea la connessione al DB
        require_once "config.php";
        $link = new mysqli(host, username, password, db);
        if (!$link) {
            // Error feedback
            $query_error = "Errore nella creazione del collegamento";
        } else {
            // Prendi gli accessi tra queste date
            $result = $link->query("SELECT * FROM accessi INNER JOIN utenti ON accessi.userId = utenti.id
                                          WHERE login BETWEEN '".$date1."' AND '".$date2."'");
            if ($result->num_rows == 0) {
                // Error feedback
                $query_error = "Non è stato trovato nessun accesso tra ".$date1." e ".$date2;
            }
        }

        // Altrimenti redirect
    } else header("Location: index.php");
    // Altrimenti redirect
} else header("Location: index.php");
?>
<html>
<head>
    <title>Elenco accessi per <?=$_GET["username"]?></title>
</head>
<body>
<?php if(isset($query_error)): ?>
    <h1><?=$query_error?></h1>
<?php else: ?>
    <h1>Elenco accessi tra <?=$date1?> e <?=$date2?></h1>
    <table>
        <tr>
            <th>Username</th><th>Login</th><th>Logout</th>
        </tr>
        <?php while($row = $result->fetch_assoc()): ?>
            <tr>
                <td><?=$row["username"]?></td><td><?=$row["login"]?></td><td><?=$row["logout"]?></td>
            </tr>
        <?php endwhile; ?>
    </table>
<?php endif; ?>
</body>
</html>
