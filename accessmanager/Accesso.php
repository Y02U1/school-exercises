<?php

require_once "config.php";

class Accesso
{
    private $id = null;
    private $userId = null;
    private $login = null;
    private $logout = null;

    public function save() {
        $link = new mysqli(host, username, password, db);
        if (!$link) {
            return false;
        }
        // FIXME: verificare parametri default in DB
        if (is_null($this->getLogout())) {
            $result = $link->query("INSERT INTO accessi(userId, login) VALUES (".
                $this->getUserId().", '".$this->getLogin()."')");
        } else {
            $result = $link->query("INSERT INTO accessi(userId, login, logout) VALUES (".
                $this->getUserId().", '".$this->getLogin()."', '".$this->getLogout()."')");
        }
        return $result;
    }

    public function lastLogin($userId)
    {
        $link = new mysqli(host, username, password, db);
        if (!$link) {
            return false;
        }
        $result = $link->query("SELECT * FROM accessi WHERE userId = $userId AND logout IS NULL");
        // FIXME: controllare il caso di sessioni appese
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $this->setId($row["id"]);
            $this->setUserId($row["userId"]);
            $this->setLogin($row["login"]);
            $this->setLogout($row["logout"]);
            return true;
        } else return false;
    }

    public function logout() {
        $link = new mysqli(host, username, password, db);
        if (!$link) {
            return false;
        }
        if (!is_null($this->getLogout())) {
            $result = $link->query("UPDATE accessi SET logout = '".$this->getLogout()."' WHERE id = ".$this->getId());
            if($result) {
                $this->setLogout($this->getLogout());
            }
            return $result;
        } else return false;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return mixed
     */
    public function getLogout()
    {
        return $this->logout;
    }

    /**
     * @param mixed $logout
     */
    public function setLogout($logout)
    {
        $this->logout = $logout;
    }

}