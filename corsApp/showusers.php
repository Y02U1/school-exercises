<?php require_once 'settings.php'; ?>
<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="style.css">
        <title>CorsApp - Mostra utenti</title>
    </head>
    <body>
        <?php if (!empty($_POST)): ?>
            <!-- Error handling -->
            <?php if (!$lines = file(FILENAME_UTENTI, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)): ?>
                <h2>Errore - apertura file!</h2>
            <?php else: ?>
                <h2>Utenti iscritti a <?php echo $_POST["corso"]; ?></h2>
                <ul>
                    <!-- Itera tra le linee -->
                    <?php foreach ($lines as $line): ?>
                        <?php
                            // Dividi i campi
                            $fields = explode(";", $line);
                            // Se il campo della materia è corretto...
                            if ($fields[1] == $_POST["corso"]) {
                                // ... stampa l'utente (l'altro campo)
                                // Proteggi da XSS https://it.wikipedia.org/wiki/Cross-site_scripting
                                echo "<li>".htmlspecialchars($fields[0])."</li>";
                            }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <a href="index.html">Torna alla dashboard</a>
            <?php endif; ?>
        <?php else: ?>
            <!-- Form per chiedere la materia da interrogare al server -->
            <h1>Scopri chi ha scelto questo corso!</h1>
            <form method="post">
                <?php require '_selectCorsi.php';?>
                <button type="submit" name="submit">Chiedi!</button>
            </form>
        <?php endif; ?>
    </body>
</html>
