<?php
// Array associativi
$corsiDocenti = [
    "Geografia" => "Stefania Ceresa",
    "Biologia" => "Beatrice Castelli",
    "Telecomunicazioni" => "Antonio Passalacqua",
    "Informatica" => "Ciro Monti"
];
$docentiCosti = [
    "Stefania Ceresa" => 100,
    "Beatrice Castelli" => 1000,
    "Antonio Passalacqua" => 5000,
    "Ciro Monti" => 50000
];
// Costante per il file
define("FILENAME_UTENTI", "utenti.csv");

// Server-side validation per i campi che verranno messi nel file
function ssv($field)
{
    // Rimuovi i CRLF e i ;
    return str_replace(array("\r", "\n", ";"), '', $field);
}
?>
